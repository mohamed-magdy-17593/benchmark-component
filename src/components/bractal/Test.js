import React from 'react'
import MediaProvider from './MediaProvider'
import withMedia from './withMedia'

function Layouts() {
  return <h1>Hola</h1>
}

function Test() {
  return (
    <div>
      <MediaProvider>
        <Layouts />
      </MediaProvider>
    </div>
  )
}

export default Test
