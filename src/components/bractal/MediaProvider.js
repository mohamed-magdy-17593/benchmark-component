/* eslint-disable react/prop-types */
import React from 'react'
import {MediaQueryProvider} from './src'

import {mediaQueryMax, mediaQueryMin, mediaQueryExact} from './cssMedia'

const queries = {
  largeDesktop: mediaQueryMin('largeDesktop'),
  maxLargeDesktop: mediaQueryMax('largeDesktop'),

  desktop: mediaQueryExact('desktop'),
  minDesktop: mediaQueryMin('desktop'),
  maxDesktop: mediaQueryMax('desktop'),

  tablet: mediaQueryExact('tablet'),
  minTablet: mediaQueryMin('tablet'),
  maxTablet: mediaQueryMax('tablet'),

  mobile: mediaQueryExact('mobile'),
  minMobile: mediaQueryMin('mobile'),
  maxMobile: mediaQueryMax('mobile'),

  minXsmall: mediaQueryMin('xsmall'),
  xsmall: mediaQueryMax('xsmall'),
}

const MediaProvider = props => (
  <MediaQueryProvider queries={queries}>{props.children}</MediaQueryProvider>
)

export default MediaProvider
