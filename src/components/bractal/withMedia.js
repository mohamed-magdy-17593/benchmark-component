import React from 'react'
import {withMedia} from './src'

export default WrappedComponent =>
  withMedia(props => {
    let patchedMedia = props.media
    return (
      <WrappedComponent {...props} media={patchedMedia}>
        {props.children}
      </WrappedComponent>
    )
  })
