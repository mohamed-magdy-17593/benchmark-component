import styled from '@emotion/styled'

const Button = styled('button')`
  min-width: 12rem;
  margin: 0 auto 20px;
  padding: 16px;
  border-radius: 5px;
  text-decoration: none;
  border: 3px solid currentColor;
  background: linear-gradient(90deg, #d26ac2, #46c9e5);
  color: #d26ac2;
  &:hover {
    opacity: 0.95;
  }
  @media (min-width: 768px) {
    margin: 0 20px 0 0;
    &:last-child {
      margin: 0;
    }
  }
`

export default Button
