import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

function Button() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm">
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
        </div>
        <div className="col-md">
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
        </div>
        <div className="col-sm">
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
          <button type="button" className="btn btn-primary">
            Primary
          </button>
        </div>
      </div>
    </div>
  )
}

export default Button
