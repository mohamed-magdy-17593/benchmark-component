import styled from '@emotion/styled'
import styledPipe, {
  detectSizes,
  detectWeights,
  detectRadius,
  detectDisabled,
  propsHave,
  TYPES,
} from '../styles/styledPipe'
import {fc, bgc, bc} from '../styles/utils/colors'
import {callTo} from '../utils/functions'

const detectColorsSchema = propsHave(callTo(bc), 'gray5', TYPES)

const Input = styled.input(
  styledPipe(
    [
      {boxSizing: 'border-box', border: '2px solid #000', width: '100%'},
      fc('gray7'),
      bgc('white'),
    ],
    detectSizes,
    detectColorsSchema,
    detectDisabled,
    detectWeights,
    detectRadius,
  ),
)

const TextArea = Input.withComponent('textarea')

export {TextArea}
export default Input
