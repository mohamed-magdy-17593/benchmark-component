/** @jsx jsx */
import * as R from 'ramda'
import {jsx, css} from '@emotion/core'
import React, {useState, useEffect, useRef} from 'react'
import ReactDOM from 'react-dom'
import styled from '@emotion/styled'
import styledPipe, {
  detectRadius,
  propsHave,
  SIZES,
  TYPES,
  SHADESTYPES,
  log,
  ifdo,
  propContain,
  replaceStyles,
} from '../styles/styledPipe'
import {
  ifFunctionCallWith,
  pickAllOmit,
  mapToKeys,
  callTo,
  functionMemo,
} from '../utils/functions'
import {p, maxw, themeW} from '../styles/utils/sizes'
import {bc, bgc} from '../styles/utils/colors'

//#region ModalBg
const detectShades = propsHave(bgc, 'simiDark', SHADESTYPES)

const ModalBg = styled.div(
  styledPipe(
    {
      position: 'fixed',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      left: 0,
      top: 0,
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      width: '100%',
      height: '100%',
      zIndex: 9999999999,
      opacity: 0,
      visibility: 'hidden',
      transform: 'scale(1.1)',
    },
    detectShades,
  ),
)

const showModalCss = css({
  opacity: 1,
  visibility: 'visible',
  transform: 'scale(1.0)',
  transition: 'visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s',
})
//#endregion

//#region ModalContent
const detectSizes = propsHave(p, 'md', SIZES)

const detectWidths = propsHave(
  callTo(maxw, themeW),
  'md',
  mapToKeys(i => `w${i}`, SIZES),
)

const detectColorSchema = propsHave(bc, 'primary', TYPES)

const ModalContent = styled.div(
  styledPipe(
    {
      border: '2px solid #fff',
      boxSizing: 'border-box',
      backgroundColor: 'white',
    },
    detectRadius,
    detectColorSchema,
    detectSizes,
    detectWidths,
  ),
)
//#endregion

//#region CloseButton Component
const detectCloseButtonColorSchema = propsHave(
  callTo(bgc, bc),
  'primary',
  TYPES,
)

const detectCloseButtonInvert = ifdo(
  propContain('invert'),
  replaceStyles('backgroundColor', 'color'),
)

const detectCloseButtonDirection = propsHave(dir => ({float: dir}), 'right', [
  'right',
  'left',
])

const CloseButtonStyles = styled.span(
  styledPipe(
    {
      width: '1.5rem',
      lineHeight: '1.5rem',
      textAlign: 'center',
      cursor: 'pointer',
      borderRadius: '0.25rem',
      color: 'white',
      border: '2px solid #fff',
    },
    detectCloseButtonColorSchema,
    detectCloseButtonInvert,
    detectCloseButtonDirection,
  ),
)

function CloseButton(props) {
  return <CloseButtonStyles {...props}>&times;</CloseButtonStyles>
}
//#endregion

const modalRoot = document.getElementById('modal-root')
function ModalPortal({children}) {
  const el = useRef(document.createElement('div'))
  useEffect(() => {
    modalRoot.appendChild(el.current)
    return () => modalRoot.removeChild(el.current)
  }, [])
  return ReactDOM.createPortal(children, el.current)
}

function createModal() {
  const openMemo = functionMemo()

  function showModal(info, modalProps) {
    const closeMemo = functionMemo()
    openMemo.handler(info, modalProps, closeMemo.handler)
    return new Promise(resolve => {
      closeMemo.save((...args) => resolve(args))
    }) // promise
  }
  function Modal({children, canClose = true, ...props}) {
    const [info, setInfo] = useState({})
    const [modalProps, setModalProps] = useState({})
    const [isShow, setIsShow] = useState(false)

    const closeFnRef = useRef(() => {})
    const close = (...args) => {
      console.log('canClose', canClose)
      if (canClose) {
        closeFnRef.current(...args)
        setIsShow(false)
      }
    }
    useEffect(() => {
      openMemo.save((info = {}, modalProps = {}, handleClose = () => {}) => {
        setInfo(info)
        setModalProps(modalProps)
        closeFnRef.current = handleClose
        setIsShow(true)
      })
    }, [])

    const [modalBgProps, modalContentProps] = pickAllOmit(SHADESTYPES, {
      ...modalProps,
      ...props,
    })

    return (
      <ModalPortal>
        <ModalBg css={isShow && showModalCss} onClick={close} {...modalBgProps}>
          <ModalContent
            onClick={e => e.stopPropagation()}
            {...modalContentProps}
          >
            {ifFunctionCallWith(children, close, info)}
          </ModalContent>
        </ModalBg>
      </ModalPortal>
    )
  }
  return [showModal, Modal]
}

export {createModal, CloseButton}
export default createModal
