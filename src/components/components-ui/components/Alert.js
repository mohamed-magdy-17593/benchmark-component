/** @jsx jsx */
import * as R from 'ramda'
import {jsx} from '@emotion/core'
import React, {useState} from 'react'
import styled from '@emotion/styled'
import styledPipe, {
  detectSizes,
  detectWeights,
  detectRadius,
  ifdo,
  propContain,
  replaceStyles,
  propsHave,
  modifyStyle,
  TYPES,
} from '../styles/styledPipe'
import {fc, bc, bgc} from '../styles/utils/colors'
import {lighten} from '../styles/utils/helpers'
import {callTo} from '../utils/functions'

const detectInvert = ifdo(propContain('invert'), [
  replaceStyles('backgroundColor', 'color'),
  fc('white'),
])

const detectColorSchema = [
  propsHave(callTo(fc, bc, bgc), 'primary', TYPES),
  modifyStyle(lighten(R.__, 0.6), 'backgroundColor'),
]

const AlertStyle = styled.div(
  styledPipe(
    {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'flex-start',
      border: '1px solid #fff',
      boxSizing: 'border-box',
    },
    detectSizes,
    detectColorSchema,
    detectInvert,
    detectWeights,
    detectRadius,
  ),
)

function Alert({children, ...props}) {
  const [show, setShow] = useState(true)
  const hide = () => setShow(false)
  return (
    show && (
      <AlertStyle {...props}>
        <div>{children}</div>
        <span
          onClick={hide}
          css={{
            cursor: 'pointer',
            fontWeight: 900,
            paddingLeft: 10,
            ':hover': {
              color: '#555',
            },
          }}
        >
          &times;
        </span>
      </AlertStyle>
    )
  )
}

export default Alert
