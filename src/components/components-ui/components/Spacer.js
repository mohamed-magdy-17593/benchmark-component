import styled from '@emotion/styled'
import {m} from '../styles/utils/sizes'

const Spacer = styled.div(props => ({
  '> *': m('xs'),
}))

export default Spacer
