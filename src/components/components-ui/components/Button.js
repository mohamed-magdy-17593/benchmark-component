/** @jsx jsx */
import {jsx} from '@emotion/core'
import * as R from 'ramda'
import styled from '@emotion/styled'
import styledPipe, {
  detectSizes,
  detectWeights,
  detectRadius,
  detectDisabled,
  TYPES,
  on,
  modifyStyle,
  propsHave,
  ifdo,
  propContain,
  replaceStyles,
} from '../styles/styledPipe'
import {fc, bgc, bc} from '../styles/utils/colors'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSpinner} from '@fortawesome/free-solid-svg-icons'
import {callTo} from '../utils/functions'
import {darken} from '../styles/utils/helpers'
import {w} from '../styles/utils/sizes'

const detectColorsSchema = propsHave(callTo(bgc, bc), 'primary', TYPES)

const detectActions = [
  on(modifyStyle(darken(R.__, 0.05), 'backgroundColor'), 'hover'),
  on(modifyStyle(darken(R.__, 0.1), 'backgroundColor'), 'active'),
  on(modifyStyle(darken(R.__, 0.07), 'backgroundColor'), 'focus'),
]

const detectInvert = ifdo(
  propContain('invert'),
  replaceStyles('backgroundColor', 'color'),
)

const detectBlock = ifdo(propContain('block'), w('100%'))

const detectPxRatio = ({pxRatio = 1}) =>
  modifyStyle(R.multiply(pxRatio), ['paddingLeft', 'paddingRight'])

const ButtonStyle = styled.button(
  styledPipe(
    [
      {position: 'relative', border: '1px solid #fff', cursor: 'pointer'},
      fc('white'),
    ],
    detectSizes,
    detectPxRatio,
    detectColorsSchema,
    detectInvert,
    detectActions,
    detectDisabled,
    detectWeights,
    detectRadius,
    detectBlock,
  ),
)

const OverCenterSpan = styled.span({
  margin: 0,
  display: 'block',
  position: 'absolute',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
})

function Button({children, loading = false, ...props}) {
  return (
    <ButtonStyle disabled={loading} {...props}>
      <span css={loading && {visibility: 'hidden'}}>{children}</span>
      {loading && (
        <OverCenterSpan>
          <FontAwesomeIcon icon={faSpinner} spin />
        </OverCenterSpan>
      )}
    </ButtonStyle>
  )
}

export default Button
