export const fw = weight => ({theme}) => ({fontWeight: theme.weights[weight]})
