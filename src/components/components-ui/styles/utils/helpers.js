import * as R from 'ramda'
import Color from 'color'

const darken = R.curry((color, ratio) =>
  Color(color)
    .darken(ratio)
    .toString(),
)
const lighten = R.curry((color, ratio) =>
  Color(color)
    .lighten(ratio)
    .toString(),
)

export {darken, lighten}
