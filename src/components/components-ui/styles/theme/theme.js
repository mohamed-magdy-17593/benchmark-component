import * as colors from './colors'
import * as sizes from './sizes'
import * as weights from './weights'

const theme = {
  colors,
  sizes,
  weights,
}

export default theme
