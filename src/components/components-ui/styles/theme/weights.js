export const light = 100
export const normal = 300
export const bold = 500
export const bolder = 700
export const exBold = 900
