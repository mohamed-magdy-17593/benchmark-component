import React from 'react'
import {ThemeProvider} from 'emotion-theming'
import theme from './theme'

function MyThemeProvider(props) {
  return <ThemeProvider theme={theme} {...props} />
}

export default MyThemeProvider
