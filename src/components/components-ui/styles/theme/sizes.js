export const spaces = {
  xs: 10 * 0.53,
  sm: 12 * 0.53,
  md: 14 * 0.53,
  lg: 16 * 0.53,
  xl: 18 * 0.53,
}

export const fontSizes = {
  xs: 10,
  sm: 12,
  md: 14,
  lg: 16,
  xl: 18,
}

export const radiuses = {
  xs: 2,
  sm: 4,
  md: 6,
  lg: 8,
  xl: 10,
  straight: 0,
  round: 5,
  fullRound: 1000,
}

export const widths = {
  xs: 300,
  sm: 500,
  md: 700,
  lg: 900,
  xl: 1200,
}
