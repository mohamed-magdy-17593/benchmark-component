import * as R from 'ramda'
import {isArray, isFunction, isObject, callTo} from '../utils/functions'
import {p, fs, br} from './utils/sizes'
import {fw} from './utils/weights'
import {lighten} from './utils/helpers'

function styledPipe(...detectors) {
  return (props, styles = {}) => {
    return self(styles, [], detectors)

    function self(styles, path, value) {
      if (isFunction(value)) {
        return self(styles, path, value(props, styles))
      }
      if (isArray(value)) {
        if (R.isEmpty(value)) {
          return styles
        }
        const [newValue, ...restValues] = value
        return self(self(styles, path, newValue), path, restValues)
      }
      if (isObject(value)) {
        if (R.isEmpty(value)) {
          return styles
        }
        const [[newPath, newValue], ...restPairs] = R.toPairs(value)
        return self(
          self(styles, [...path, newPath], newValue),
          path,
          R.fromPairs(restPairs),
        )
      }
      return R.assocPath(path, value, styles)
    }
  }
}

// -- styledPipe helpers --

// for debug
export const log = a => {
  console.log(a)
  return a
}

export const propsHave = R.curry((cb, defaultx, xs) => (props, styles) =>
  cb(
    ((isObject(xs) ? R.toPairs(xs) : xs.map(x => [x, x])).filter(
      ([key, _]) => props[key],
    )[0] || [, defaultx])[1], // eslint-disable-line
    props,
    styles,
  ),
)

export const replaceStyles = R.curry((style1, style2) => (_, styles) => ({
  [style1]: styles[style2],
  [style2]: styles[style1],
}))

export const ifdo = R.curry((cb, styleDetector) => (props, styles) =>
  cb(props, styles) ? styleDetector : [],
)

export const propContain = (...strs) => props => strs.some(s => props[s])

export const on = R.curry((cb, actions) =>
  R.flatten([actions]).map(action => ({[`:${action}`]: cb})),
)

export const modifyStyle = R.curry(
  (modificationFn, stylesToModify) => (_, styles) =>
    R.flatten([stylesToModify]).map(prop => ({
      [prop]: modificationFn(styles[prop]),
    })),
)

const isSubOf = R.curry(
  (o1, o2) =>
    isObject(o1) &&
    isObject(o2) &&
    R.toPairs(o1).every(([key, value]) =>
      isObject(value) ? isSubOf(value, o2[key]) : value === o2[key],
    ),
)

const stylesChangeErrorHandling = cb => (styleDetector, msg = 'Error') => (
  _,
  stylesBefore,
) => [
  styleDetector,
  (_, stylesAfter) => {
    if (cb(stylesBefore, stylesAfter)) {
      console.log(
        `%c${msg}` +
          '\n' +
          '%cStyles Before:' +
          '\n' +
          `%c${JSON.stringify(stylesBefore, null, 2)}` +
          '\n' +
          '%cStyles After:' +
          '\n' +
          `%c${JSON.stringify(stylesAfter, null, 2)}`,
        'color: white; background: red; font-size: 20px; margin: 5px 0;',
        'color: white; background: red; font-size: 15px; margin: 5px 0;',
        'background: black; color: white',
        'color: white; background: red; font-size: 15px; margin: 5px 0;',
        'background: black; color: white',
      )
    }
    return []
  },
]

export const shouldOverWriteStyle = stylesChangeErrorHandling((s1, s2) =>
  isSubOf(s1, s2),
)
export const shouldNotOverWriteStyle = stylesChangeErrorHandling(
  (s1, s2) => !isSubOf(s1, s2),
)

// -- utils --

export const TYPES = [
  'primary',
  'secondary',
  'success',
  'danger',
  'error',
  'info',
  'warning',
]

export const SHADESTYPES = ['lighter', 'light', 'dark', 'simiDark']

export const SIZES = ['xs', 'sm', 'md', 'lg', 'xl']

export const WEIGHTS = ['light', 'normal', 'bold', 'bolder', 'exBold']

export const RADIUSES = ['straight', 'round', 'fullRound']

export const detectSizes = propsHave(callTo(p, fs), 'md', SIZES)

export const detectWeights = propsHave(callTo(fw), 'md', WEIGHTS)

export const detectRadius = propsHave(callTo(br), 'round', RADIUSES)

export const detectDisabled = ifdo(propContain('disabled'), [
  modifyStyle(lighten(R.__, 0.5), ['backgroundColor', 'borderColor', 'color']),
  on(null, ['hover', 'active', 'focus']),
])

export default styledPipe
