const id = (_, x) => x

export const isArray = Array.isArray

export const isString = x => typeof x === 'string'

export const isFunction = x => typeof x === 'function'

export const isObject = x =>
  !!x && Object.prototype.toString.call(x) === '[object Object]'

const callIfFunction = (x, ...args) => (isFunction(x) ? x(...args) : x)

export const addStringWith = s => (v1, v2) => `${v1}${s}${v2}`

export const T = () => true

export const cond = condArr => (...args) => {
  const [, f] = condArr.find(([p]) => p(...args)) || []
  return callIfFunction(f, ...args)
}

const objAssing = o => k => v => ({...o, [k]: v})

export const emptyObjectAssing = objAssing({})

export const reduce = f => i => xs => xs.reduce(f, i)

export const mergeWith = f => (o1, o2) =>
  Object.entries(o2).reduce(
    (acc, [k, v]) => ({...acc, [k]: acc[k] ? f(acc[k], v) : v}),
    o1,
  )

export const merge = mergeWith(id)
