import classs, {composeClasssR} from './index'

function styless(el) {
  return (...classsProp) => ({children, ...props}) =>
    classs(
      el,
      composeClasssR({...props, classs: classsProp}),
      ...[].concat(children),
    )
}

export default styless
