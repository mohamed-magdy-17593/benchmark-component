import styless from '../classs/styless'
import 'bootstrap/dist/css/bootstrap.min.css'

function callIfFunction(x, ...args) {
  return typeof x === 'function' ? x(...args) : x
}

function classsMap(objMap) {
  return props =>
    Object.entries(objMap).map(
      ([prop, value]) =>
        props[prop] && callIfFunction(value, props[prop], props),
    )
}

const detectAppearance = ({appearance = 'primary', outline = false}) =>
  outline ? `btn-outline-${appearance}` : `btn-${appearance}`

const StyledButton = [
  'btn',
  detectAppearance,
  classsMap({
    size: size => `btn-${size}`,
    disabled: 'disabled',
    block: 'btn-block',
    active: 'active',
  }),
]

const Button = styless('button')(StyledButton)
const ButtonLink = styless('a')(StyledButton)

export {Button, ButtonLink}
