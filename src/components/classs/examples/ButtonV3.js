import styless from '../classs/styless'
import 'bootstrap/dist/css/bootstrap.min.css'

function propsContain(types, cb, defaultType) {
  defaultType = defaultType || types[0]
  return props => {
    const type = types.find(type => props[type] && type) || defaultType
    return cb(type, props)
  }
}

const appearances = [
  'primary',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'light',
  'dark',
  'link',
]

// prettier-ignore
const sizes = [
  'md',
  'sm',
  'lg'
]

const detectAppearance = propsContain(appearances, (appearance, {outline}) =>
  outline ? `btn-outline-${appearance}` : `btn-${appearance}`,
)

const detectSize = propsContain(sizes, size => `btn-${size}`)

const detectBlock = ({block}) => block && 'btn-block'

const detectActive = ({active}) => active && 'active'

const detectDisabled = ({disabled}) => disabled && 'disabled'

const StyledButton = [
  'btn',
  detectAppearance,
  detectSize,
  detectBlock,
  detectActive,
  detectDisabled,
]

const Button = styless('button')(StyledButton)
const ButtonLink = styless('a')(StyledButton)

export {Button, ButtonLink}
