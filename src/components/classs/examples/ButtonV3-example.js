/* @jsx classs */
import classs from '../classs' // eslint-disable-line
import React from 'react'
import {Button, ButtonLink} from './ButtonV3'

function Example() {
  return (
    <div>
      <Button>Click</Button>
      <Button danger>Click</Button>
      <Button success outline>
        Click
      </Button>
      <Button secondary lg>
        Click
      </Button>
      <Button secondary sm>
        Click
      </Button>
      <Button success sm active>
        active
      </Button>
      <Button dark sm>
        not active
      </Button>
      <Button sm disabled>
        Click
      </Button>
      <Button block>Click</Button>
      <hr />
      <ButtonLink href="#">Click</ButtonLink>
      <ButtonLink href="#" danger>
        Click
      </ButtonLink>
      <ButtonLink href="#" success outline>
        Click
      </ButtonLink>
      <ButtonLink href="#" secondary lg>
        Click
      </ButtonLink>
      <ButtonLink href="#" secondary sm>
        Click
      </ButtonLink>
      <ButtonLink href="#" sm active>
        Click
      </ButtonLink>
      <ButtonLink href="#" sm disabled>
        Click
      </ButtonLink>
      <ButtonLink href="#" block>
        Click
      </ButtonLink>
      <hr />
      <ButtonLink classs={layouts} col="12" href="#">
        Click
      </ButtonLink>
    </div>
  )
}

const layouts = ({col, size = 'md'}) => `col-${size}-${col}`

export default Example
