/* @jsx classs */
import classs from '../classs' // eslint-disable-line
import React from 'react'
import {Button, ButtonLink} from './ButtonV2'

function Example() {
  return (
    <div>
      <Button>Click</Button>
      <Button appearance="danger">Click</Button>
      <Button appearance="success" outline>
        Click
      </Button>
      <Button appearance="secondary" size="lg">
        Click
      </Button>
      <Button appearance="secondary" size="sm">
        Click
      </Button>
      <Button appearance="success" size="sm" active>
        active
      </Button>
      <Button appearance="success" size="sm">
        not active
      </Button>
      <Button size="sm" disabled>
        Click
      </Button>
      <Button block>Click</Button>
      <hr />
      <ButtonLink href="#">Click</ButtonLink>
      <ButtonLink href="#" appearance="danger">
        Click
      </ButtonLink>
      <ButtonLink href="#" appearance="success" outline>
        Click
      </ButtonLink>
      <ButtonLink href="#" appearance="secondary" size="lg">
        Click
      </ButtonLink>
      <ButtonLink href="#" appearance="secondary" size="sm">
        Click
      </ButtonLink>
      <ButtonLink href="#" size="sm" active>
        Click
      </ButtonLink>
      <ButtonLink href="#" size="sm" disabled>
        Click
      </ButtonLink>
      <ButtonLink href="#" block>
        Click
      </ButtonLink>
      <hr />
      <ButtonLink classs={layouts} col="12" href="#">
        Click
      </ButtonLink>
    </div>
  )
}

const layouts = ({col, size = 'md'}) => `col-${size}-${col}`

export default Example
