import styless from '../classs/styless'
import 'bootstrap/dist/css/bootstrap.min.css'

const detectAppearance = ({appearance = 'primary', outline = false}) =>
  outline ? `btn-outline-${appearance}` : `btn-${appearance}`

const detectSize = ({size = 'md'}) => `btn-${size}`

const detectBlock = ({block}) => block && 'btn-block'

const detectActive = ({active}) => active && 'active'

const detectDisabled = ({disabled}) => disabled && 'disabled'

const StyledButton = [
  'btn',
  detectAppearance,
  detectSize,
  detectBlock,
  detectActive,
  detectDisabled,
]

const Button = styless('button')(StyledButton)
const ButtonLink = styless('a')(StyledButton)

export {Button, ButtonLink}
