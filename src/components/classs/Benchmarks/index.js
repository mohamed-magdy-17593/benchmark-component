// yarn add cuid ramda
import React, {Component, unstable_Profiler as Profiler} from 'react'
import cuid from 'cuid'
import * as R from 'ramda'
import onComplete from './onComplete'

let f

function logProfile(
  id,
  phase,
  actualTime,
  baseTime,
  startTime,
  commitTime,
  interactions,
) {
  f({
    [id]: {
      [phase]: [
        {
          actualTime,
          baseTime,
          startTime,
          commitTime,
          interactions,
          defrance: commitTime - startTime,
        },
      ],
    },
  })
}

// deep merge helper
const arrMergeDeep = R.mergeDeepWith(R.concat)

const propsTotal = props => objects =>
  objects.reduce(
    (objectsAcc, o) =>
      props.reduce(
        (acc, prop) => ({
          ...acc,
          [prop]: (acc[prop] || 0) + o[prop],
        }),
        objectsAcc,
      ),
    {},
  )

const profilerTotal = propsTotal(['actualTime', 'baseTime', 'defrance'])

class ExperimentResult extends Component {
  static defaultProps = {
    onChange() {},
  }
  state = {results: {}}
  componentDidMount() {
    f = newResult =>
      this.setState(
        ({results}) => ({
          results: arrMergeDeep(results, newResult),
        }),
        () => {
          this.props.onChange(this.state.results)
        },
      )
  }
  render() {
    const {results} = this.state
    return (
      <div>
        {R.toPairs(results).map(([id, phases]) => (
          <div key={id}>
            <h3>{id}</h3>
            <table style={{width: '100%'}}>
              <thead>
                <tr>
                  <th>phase</th>
                  <th>data</th>
                  <th>calculations</th>
                </tr>
              </thead>
              <tbody>
                {R.toPairs(phases).map(([phaseName, phaseData], i) => {
                  const len = phaseData.length
                  const total = profilerTotal(phaseData)
                  return (
                    <tr key={i}>
                      <th valign="top">{phaseName}</th>
                      <td valign="top">
                        <table>
                          <thead>
                            <tr>
                              <th />
                              <th>actualTime</th>
                              <th>baseTime</th>
                              <th>startTime</th>
                              <th>commitTime</th>
                              <th>defrance</th>
                            </tr>
                          </thead>
                          <tbody>
                            {phaseData.map((d, i) => (
                              <tr key={i}>
                                <td valign="top">{i + 1}</td>
                                <td valign="top">{d.actualTime}</td>
                                <td valign="top">{d.baseTime}</td>
                                <td valign="top">{d.startTime}</td>
                                <td valign="top">{d.commitTime}</td>
                                <td valign="top">{d.defrance}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </td>
                      <td valign="top">
                        <table>
                          <thead>
                            <tr>
                              <th />
                              <th>actualTime</th>
                              <th>baseTime</th>
                              <th>defrance</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th valign="top">total</th>
                              <td
                                valign="top"
                                className={`${phaseName} total actualTime`}
                              >
                                {total.actualTime}
                              </td>
                              <td
                                valign="top"
                                className={`${phaseName} total baseTime`}
                              >
                                {total.baseTime}
                              </td>
                              <td
                                valign="top"
                                className={`${phaseName} total defrance`}
                              >
                                {total.defrance}
                              </td>
                            </tr>
                            <tr>
                              <th valign="top">avg</th>
                              <td
                                valign="top"
                                className={`${phaseName} avg actualTime`}
                              >
                                {total.actualTime / len}
                              </td>
                              <td
                                valign="top"
                                className={`${phaseName} avg baseTime`}
                              >
                                {total.baseTime / len}
                              </td>
                              <td
                                valign="top"
                                className={`${phaseName} avg defrance`}
                              >
                                {total.defrance / len}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        ))}
      </div>
    )
  }
}

const range = num => Array.from({length: num}, (_, i) => i)
const generator = arr => () => arr.map(cuid)

/**
 * this component contain profiler
 * this is the main component that Children logic
 */
class ExperimentPlayground extends React.PureComponent {
  static defaultProps = {
    numOfRenderdComponent: 100,
    numOfReRenders: 10,
    reRenderIntervar: 1000,
    profilerName: 'unknown',
  }
  keyGenerator = generator(range(this.props.numOfRenderdComponent))
  state = {keys: this.keyGenerator()}
  updateKeys = () => this.setState({keys: this.keyGenerator()})
  _count = 0
  update() {
    const {numOfReRenders, reRenderIntervar} = this.props
    setTimeout(() => {
      if (this._count < numOfReRenders) {
        this._count++
        this.updateKeys()
        this.update()
      } else {
        alert('Completed!')
        onComplete(this.props)
      }
    }, reRenderIntervar)
  }
  componentDidMount() {
    this.update()
  }
  render() {
    const {keys} = this.state
    return (
      <Profiler id={this.props.profilerName} onRender={logProfile}>
        {keys.map(id => React.cloneElement(this.props.children, {key: id}))}
      </Profiler>
    )
  }
}

/**
 * using this main Component
 * using it like
 * <Benchmarks
 *   numOfRenderdComponent={100}, // default
 *   numOfReRenders={10}, // default
 *   reRenderIntervar={1000}, // default
 * >
 *   <Test id="uniqueId1" />
 *   <Test id="uniqueId2" />
 *   <Test id="uniqueId3" />
 *     ...
 *     ...
 * </Benchmarks>
 */
function Benchmarks(props) {
  return (
    <>
      <ExperimentResult />
      <hr style={{border: '5px solid black', margin: '30px 0 '}} />
      <h2 style={{textAlign: 'center', margin: '12px 0'}}>
        Experiment Playground
      </h2>
      <ExperimentPlayground {...props} />
    </>
  )
}

export default Benchmarks
