const $ = document.querySelector.bind(document)
function getTextOf(query) {
  return $(query).innerText
}

export const url = 'http://localhost:8080/'
function postToServer(data) {
  return fetch(url, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  }).then(res => res.json())
}

async function onComplete({
  profilerName,
  numOfReRenders,
  numOfRenderdComponent,
  reRenderIntervar,
}) {
  const info = {
    profilerName,
    numOfReRenders,
    numOfRenderdComponent,
    reRenderIntervar,
    total: {
      actualTime: getTextOf('.update.total.actualTime'),
      baseTime: getTextOf('.update.total.baseTime'),
      defrance: getTextOf('.update.total.defrance'),
    },
    avg: {
      actualTime: getTextOf('.update.avg.actualTime'),
      baseTime: getTextOf('.update.avg.baseTime'),
      defrance: getTextOf('.update.avg.defrance'),
    },
  }
  const response = await postToServer(info)
  console.log({response})
}

export default onComplete
