/* @jsx classs */
import classs from './classs' // eslint-disable-line
import React from 'react'
import Benchmarks from './Benchmarks'
// import Ex from './examples/ButtonV1-example'
// import Ex from './examples/ButtonV2-example'
import Ex from './examples/ButtonV3-example'

const profilerName = 'ButtonV3-example props as value implementation'
const numOfRenderdComponent = 100
const numOfReRenders = 10
const reRenderIntervar = 1000

function App() {
  return (
    <Benchmarks
      numOfRenderdComponent={numOfRenderdComponent}
      numOfReRenders={numOfReRenders}
      reRenderIntervar={reRenderIntervar}
      profilerName={profilerName}
    >
      <Ex />
    </Benchmarks>
  )
}

export default App
