import React from 'react'
import {Button, ButtonLink} from './Button'

function Example() {
  return (
    <div>
      <Button>Click</Button>
      <Button appearance="danger">Click</Button>
      <Button appearance="success" outline>
        Click
      </Button>
      <Button appearance="warning" size="lg">
        Click
      </Button>
      <Button appearance="warning" size="xs">
        Click
      </Button>
      <Button appearance="success" size="xs" active>
        active
      </Button>
      <Button appearance="success" size="xs">
        not active
      </Button>
      <Button size="xs" disabled>
        Click
      </Button>
      <Button block>Click</Button>
      <hr />
      <ButtonLink href="#">Click</ButtonLink>
      <ButtonLink href="#" appearance="danger">
        Click
      </ButtonLink>
      <ButtonLink href="#" appearance="success" outline>
        Click
      </ButtonLink>
      <ButtonLink href="#" appearance="warning" size="lg">
        Click
      </ButtonLink>
      <ButtonLink href="#" appearance="warning" size="xs">
        Click
      </ButtonLink>
      <ButtonLink href="#" size="xs" active>
        Click
      </ButtonLink>
      <ButtonLink href="#" size="xs" disabled>
        Click
      </ButtonLink>
      <ButtonLink href="#" block>
        Click
      </ButtonLink>
      <hr />
    </div>
  )
}

export default Example
