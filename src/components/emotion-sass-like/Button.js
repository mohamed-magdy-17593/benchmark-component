import styled from '@emotion/styled'
import * as buttonCss from './buttonCss'

const Button = styled.button`
  ${buttonCss.btn}
  ${({appearance = 'primary', ...restProps}) =>
    buttonCss.appearances[appearance](restProps)}
  ${({block}) => block && buttonCss.block}
  ${({size = 'md'}) => buttonCss.sizes[size]}
`

const ButtonLink = Button.withComponent('a')

export {Button, ButtonLink}
