const appearanceTypes = {
  success: '#11cc00',
  danger: '#dd1010',
  primary: '#45b1e5',
  warning: '#ffe52b',
  info: '#6bddef',
}

export {appearanceTypes}
