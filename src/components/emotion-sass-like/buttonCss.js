import {css} from '@emotion/core'
import {darken, lighten} from './utils'
import {appearanceTypes} from './appearanceTypes'
import {sizeTypes} from './sizeTypes'

function each(types, cb) {
  return Object.entries(types).reduce(
    (acc, keyValuePairs) => ({
      ...acc,
      ...cb(...keyValuePairs),
    }),
    {},
  )
}

const block = css`
  width: 100%;
`

const btn = css`
  color: black;
  padding: 10px 20px;
  border-radius: 5px;
  box-shadow: rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: 0.3s all ease;
  cursor: pointer;
`

const sizes = each(sizeTypes, (sizeType, {font, space}) => ({
  [sizeType]: css`
    font-size: ${font}px;
    padding: ${space}px ${space * 2}px;
    line-height: ${1.5 * font}px;
  `,
}))

const appearances = each(appearanceTypes, (appearanceType, color) => ({
  [appearanceType]: ({outline, active, disabled}) => css`
    color: white;
    background-color: ${color};
    border: 1px solid ${color};
    &:hover {
      background-color: ${darken(color, 0.15)};
    }

    ${outline &&
      css`
        background-color: white;
        border: 1px solid ${color};
        color: ${color};
        &:hover {
          color: white;
        }
      `}

    ${active &&
      css`
        background-color: ${darken(color, 0.15)};
        ${outline &&
          css`
            background-color: ${color};
            color: white;
          `}
      `}

    ${disabled &&
      css`
        cursor: not-allowed;
        background-color: ${lighten(color, 0.3)};
        border: 1px solid ${lighten(color, 0.3)};
        ${outline &&
          css`
            cursor: not-allowed;
            background-color: white;
            color: ${lighten(color, 0.3)};
            &:hover {
              background-color: #fcfcfc;
            }
          `}
      `}
  `,
}))

export {block, btn, appearances, sizes}
