const sizeTypes = {
  xs: {
    font: 11,
    space: 6,
  },
  md: {
    font: 14,
    space: 10,
  },
  lg: {
    font: 24,
    space: 18,
  },
}

export {sizeTypes}
