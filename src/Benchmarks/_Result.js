import React, {Component} from 'react'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts'

import {url} from './_onComplete'

function CustomizedAxisTick(props) {
  const {x, y, payload} = props

  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dy={16}
        textAnchor="end"
        fill="#666"
        transform="rotate(-60)"
      >
        {payload.value}
      </text>
    </g>
  )
}

function Chart({results}) {
  return (
    <LineChart
      width={1600}
      height={600}
      data={results}
      margin={{top: 5, right: 30, left: 20, bottom: 5}}
    >
      <XAxis
        dataKey="profilerName"
        height={200}
        tick={<CustomizedAxisTick />}
      />
      <YAxis />
      <CartesianGrid strokeDasharray="3 3" />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="avg.actualTime"
        stroke="#3498db"
        activeDot={{r: 8}}
      />
      <Line
        type="monotone"
        dataKey="avg.baseTime"
        stroke="#9b59b6"
        activeDot={{r: 8}}
      />
      <Line
        type="monotone"
        dataKey="avg.defrance"
        stroke="#1abc9c"
        activeDot={{r: 8}}
      />
    </LineChart>
  )
}

class Result extends Component {
  state = {results: null}
  async componentDidMount() {
    const results = await fetch(url).then(res => res.json())
    console.log({results})
    this.setState({results})
  }
  render() {
    const {results} = this.state
    return (
      results && (
        <div
          style={{
            textAlign: 'center',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <section>
            <h2>Charts</h2>
            <Chart results={results} />
          </section>
        </div>
      )
    )
  }
}

export default Result
