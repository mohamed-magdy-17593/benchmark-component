import React from 'react'

// import Benchmarks from './Benchmarks'
// import Test from './components/components-ui/Test'
// import Test from './components/styled-components/Button'
// import Test from './components/emotion/Button'
// import Test from './components/normal-css/Button'
// import Test from './components/bootstrap/Button'
import Test from './components/emotion-sass-like/Test'
// import Test from './components/classs/examples/ButtonV1-example'
// import Test from './components/classs/examples/ButtonV2-example'
// import Test from './components/classs/examples/ButtonV3-example'

// const profilerName = 'ButtonV1-example'
// const numOfRenderdComponent = 100
// const numOfReRenders = 10
// const reRenderIntervar = 1000

function App() {
  return <Test />
}

export default App
